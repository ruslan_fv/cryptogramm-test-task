//todo complete the request

$(document).ready(function () {
    //will be removed when request will work
    $(".submit").click(function () {
        $(".popup-sign-success").toggleClass("display");
        $(".mask").toggleClass("display");
        var wsUrl = "https://dssclients.taxnet.ru/CryptxDSS/Services/CryptoOperation.svc?singleWsdl?op=Sign";

        var soapRequest =
            '<?xml version="1.0" encoding="utf-8"?> \
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
                xmlns:xsd="http://www.w3.org/2001/XMLSchema" \
                xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
              <soap:Body> \
                <Sign xmlns=""> \
                  <name>' + $("").val() + '</name> \
                </Sign> \
              </soap:Body> \
            </soap:Envelope>';

        $.ajax({
            type: "POST",
            url: wsUrl,
            contentType: "text/xml",
            dataType: "xml",
            data: soapRequest,
            success: processSuccess,
            error: processError
        });

    });
});

function processSuccess(data, status, req) {
    $(".popup-sign-success").toggleClass("display");
    $(".mask").toggleClass("display");
    //todo display processed files in the popup
}

function processError(data, status, req) {
    alert(req.responseText + " " + status);
}