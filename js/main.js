$(document).ready(function () {
    $(".operation").click(function () {
        $(".operation").removeClass("selected");
        $(this).toggleClass("selected");
        if ($(".selected").hasClass("sign")) {
            sign_type.addClass("display");
        } else {
            if (sign_type.hasClass("display")) {
                sign_type.removeClass("display");
            }
        }
        $(".submit").prop("disabled", false);
    });

    $('.upload-file').change(
        function () {
            //todo get from file
            var fileblock = "<div class=\"file checked\">\n" +
                "    <div class=\"checkbox\"></div>\n" +
                "    <div class=\"txt-logo logo\"></div>\n" +
                "    <div class=\"file-descr\">\n" +
                "        <p class=\"name\">" + $(this).val().split('\\').pop() + "</p>\n" +
                "        <p class=\"size\">" + "30 байт" + "</p>\n" +
                "        <p class=\"date\">от 13.10.2017</p>\n" +
                "    </div>\n" +
                "</div>";
            uploaded_files.html(uploaded_files.html() + fileblock);
            $(".op-type").addClass("display");
        });

    var sign_type = $(".sign-type"),
        uploaded_files = $(".col-12 .uploaded-files");

    $(".checkbox").click(function () {
        $(this).parent().toggleClass("checked");
    });

    $(".toggler-wrapper").mousedown(function () {
        $(".toggler-pos").toggleClass("right");
    });

    $(".dropdown").click(function () {
        $(".dropdown-list").toggleClass("display");
    });

    $(".dropdown-list .item").click(function () {
       $(".dropdown-list").toggleClass("display");
    });

    $(".close-popup").click(function () {
       $(this).parent().toggleClass("display");
       $(".mask").toggleClass("display");
    });
});